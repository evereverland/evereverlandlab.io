# Why are we doing this?


## Suggested 'calls to action'

* Do you want to live forever?
* The Search for Eternal Life, through data collecting and tracking
* Death 2.0 - interaction with your death
* New homes for old things.

* Creating a system that helps people do the metadata work to make this process flow smoother


## What is the EverEverland mission and vision?

What are a dozen sentences or so would be nice that cover what we want to do now (the mission ) and a some more about where we see the project providing some future benefit or nicety ( the vision )

Can we all suggest one or two or more thoughts?

### Mission

* To capture, preserve, index and make readily available forever at no charge to the people of the future the data created by the people of today
* "Your grandmother has a great recipe for preserves? Great!. We can preserve it with no expiry date. For one dollar per page."
* Develop and document methods, standards and tools usable by anybody
	* Fingers crossed, business develops and we get customers
* Get everything online, indexed and viewable
	* API nice
	* Two or more long term cloud storage places
* Keep it simple....
* We are: Thoughtful/Local/Practical


### Vision

* Peeps know more about each other
* Can we create a product that is a more user friendly model than Evernote?


And

_We need to continue Mark's questioning regarding the meaning of all this_



***

# <center title="hello!" ><a href=javascript:window.scrollTo(0,0); style=text-decoration:none; > ❦ </a></center>

