<span style=display:none; >[You are now in a GitLab source code view - click this link to view Read Me file as a web page]( https://evereverland.gitlab.io/ "View file as a web page." ) </span>

# EverEverLand Read Me
Patrick edited this docement!

## What this website is about

This website is about ever ever land, a project created by for people with the intention of helping themselves and others to archive public and personal data that could be of interest to future historians and future people in general.

fingers crossed that this project actually becomes a real start up. there is already the first paying customer - one of the founders.


## How this website is built

This website is built around a very rudimentary content management system written entirely in plain vanilla JavaScript and hosted on gitlab.

The goal is that all project members may be able to add and edit all the material in this website. it should enable both the new user and the expert user to add value in their own ways.

## What this website provides

* Notes of meetings and other time related posts
* Links of Interest
* hosting for online scripts that may be of use or just for the fun of it
* and any papers and documets that may be of interest to the project